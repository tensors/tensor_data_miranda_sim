%% load data (permute and flip for viz) and set global parameters
load density
density = permute(density,[3 2 1]);
density = flip(density,3);
density = flip(density,1);
limits = [min(density,[],'all') max(density,[],'all')];

%% create 3D viz using slice
figure, h = slice(density,1,1,2048); 
config_slice(h,gcf,limits); % local function defined below
exportgraphics(gcf,'graphics/sliceviz.png');

%% create xy slice viz manually
zs = 64:384:2048;
figure, tiledlayout(1,length(zs),'TileSpacing','Compact');
for z = zs
    nexttile, imagesc(density(:,:,z));
    axis square
    clim(limits)
    title('z = ' + string(z))
end
colorbar
height = 3; width = 2*length(zs)*height; 
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100], 'Colormap', jet);
exportgraphics(gcf,'graphics/xy-slices.png');

%% create xy slice viz using slice
figure, h = slice(density,[],[],zs);
config_slice(h,gcf,limits); % local function defined below
exportgraphics(gcf,'graphics/xy-sliceviz.png');

%% create xz slice viz manually
ys = 16:48:256;
figure, tiledlayout(1,length(ys),'TileSpacing','Compact');
for y = ys
    nexttile, imagesc(rot90(squeeze(density(:,y,:))));
    clim(limits)
    title('y = ' + string(y))
end
colorbar
height = 4; width = .5*length(ys)*height; 
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100], 'Colormap', jet);
exportgraphics(gcf,'graphics/xz-slices.png');

%% create xz slice viz using slice
figure, h = slice(density,[],ys,[]);
config_slice(h,gcf,limits); % local function defined below
exportgraphics(gcf,'graphics/xz-sliceviz.png');

%% create yz slice viz manually
xs = 16:48:256;
figure, tiledlayout(1,length(xs),'TileSpacing','Compact');
for x = xs
    nexttile, imagesc(rot90(squeeze(density(x,:,:))));
    clim(limits)
    title('x = ' + string(x))
end
colorbar
height = 4; width = .5*length(xs)*height; 
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100], 'Colormap', jet);
exportgraphics(gcf,'graphics/yz-slices.png');

%% create yz slice viz using slice
figure, h = slice(density,xs,[],[]);
config_slice(h,gcf,limits); % local function defined below
exportgraphics(gcf,'graphics/yz-sliceviz.png');

%% function to make slice viz uniform
function config_slice(hdl,fig,limits)
    set(hdl,'EdgeColor','none');
    axis([0 256 0 256 0 2048]);
    clim(limits);
    width = 10; height = 80;
    pos = get(fig, 'Position');
    set(fig, 'Position', [pos(1) pos(2) width*100, height*100], 'Colormap', jet);
    view(-45,15);
end