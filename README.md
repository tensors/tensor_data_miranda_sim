# Miranda Turbulent Flow Dataset

## How to Cite

* G. Ballard, T. G. Kolda, and P. Lindstrom, Miranda Turbulent Flow Dataset, https://gitlab.com/tensors/tensor_data_miranda_sim, 2022.
* K. Zhao, S. Di, X. Lian, S. Li, D. Tao, J. Bessac, Z. Chen, and F. Cappello, SDRBench: Scientific Data Reduction Benchmark for Lossy Compressors, 2020 IEEE International Conference on Big Data (Big Data), 2020, https://doi.org/10.1109/bigdata50022.2020.9378449 
* W. H. Cabot, and A. W. Cook, Reynolds number effects on Rayleight-Taylor instability with possible implications for type Ia supernovae, Nature Physics, Vol. 2, No. 8, pp. 562-568, July 2006, https://doi.org/10.1038/nphys361

## Description

The **Miranda Turbulent Flow** tensor data is one dataset from the [Scientific Data Reduction Benchmark (SDRBench)](https://sdrbench.github.io/).
The simulation was computed using [Miranda](https://wci.llnl.gov/simulation/computer-codes/miranda), which is software from Lawrence Livermore National Laboratory (LLNL) for simulating flows with turbulent mixing. 
This dataset corresponds to a late time step of a Rayleigh-Taylor instability direct numerical simulation run on a uniform grid with periodic boundary conditions.  
The initial conditions are a slightly perturbed planar interface between two fluids of relative density 1 and 3.
There are two versions of the data available: a small 4D tensor capturing 7 fields (e.g., density, pressure, viscosity) of information on a coarsened 384 x 384 x 256 grid and a big 3D tensor capturing 1 field (density) on the original 3072 x 3072 x 3072 grid.

To obtain a dataset of manageable size (1 GB when loaded into memory), we extract a subtensor from the large version of the data.
That is, we take a 256 x 256 x 2048 subtensor from the middle of the density grid.
We use a larger dimension in the 3rd mode in order to capture the variations across the z direction, ignoring many of the xy slices that are purely one fluid or the other.
Using Matlab-style indexing, the indices of the extracted tensor are `[1408:1663,1408:1663,512:2559]`.
While the original data is stored in single precision, we convert to double precision for more uniform processing in Matlab.
We also permute the dimensions so that the z direction is first, which corresponds to the vertical direction in our visualizations.

The tensor is formatted as
**2048 z grid points x 256 x grid points x 256 y grid points**.

## Files and Variables

* `density.mat` (311 MB) - Contains the 2048 x 256 x 256 multidimensional array `density` in double precision.
* `vizmiranda.m` - Contains a Matlab script to generate the figures displayed in this README.
* `hosvd_examples.m` - Contains a Matlab script to compute several Tucker decompositions and display the results.

The files are tracked via GIT LFS (https://git-lfs.github.com/). If you checkout this repo, you have to execute
`git lfs fetch origin main` after cloning to pull the mat file using `git lfs pull`.

## Visualization of Tensor 

A volume-rendering of the density in the original domain with pure voxels removed, from P. Lindstrom (Fixed-Rate Compressed Floating-Point Arrays, IEEE Trans. Visualization and Computer Graphics, Vol. 20, No. 12, pp. 2674-2683, 2014, doi:10.1109/TVCG.2014.2346458) is as follows:

![Visualization of X(1,:,:,:)](graphics/original-data-3d.png "Volume rendering of density field")

We can also use Matlab visualization functions `slice` and `imagesc` to visualize 2D slices of the extracted tensor.

<img src="graphics/sliceviz.png" width="450px" alt="3D Visualization of Density Subtensor">

Below are 256 x 256 horizontal slices for various values of z, shown separately and displayed in place in three dimensions.

<img src="graphics/xy-slices.png" width="600px" alt="xy Slices">

<img src="graphics/xy-sliceviz.png" width="450px" alt="3D xy Slices">


Below are 2048 x 256 lateral slices for various values of y.

<img src="graphics/xz-slices.png" width="600px" alt="xz Slices">

<img src="graphics/xz-sliceviz.png" width="450px" alt="3D xz Slices">

Below are 2048 x 256 frontal slices for various values of x.

<img src="graphics/yz-slices.png" width="600px" alt="yz Slices">

<img src="graphics/yz-sliceviz.png" width="450px" alt="3D yz Slices">

## Computing Tucker and Visualization of Compressed Results


We compute four different ST-HOSVD compressions with threshold 10^(-1), 10^(-2), 10^(-3), and 10^(-4). The code is `hosvd_examples.m`.
The compression ratios range from 5X (19% of the original data size) to 4,4775X (0.02% of the original data size). 


| Threshold | Rel. Error | Core Size | Compression Ratio | % of Original Size |
|-----------|-------|-----------|------------------|------------|
| 1e-1 | 8.8e-2 | 13 x 3 x 2 | 4775 | 0.02 |
| 1e-2 | 9.8e-3 | 232 x 43 x 41 | 148 | 0.7 |
| 1e-3 | 9.8e-4 | 583 x 102 x 99 | 19 | 5 |
| 1e-4 | 9.8e-5 | 934 x 161 x 158 | 5 | 19 |

We plot below the results. The relative error is in terms of sum of squared error, i.e., ||X-T||^2 / ||X||^2.
The relative compression is the ratio of the total size of X (i.e., the product of its dimension)
and the total storage required for the Tucker approximation (i.e., the product of the core dimensions plus the sum of the products of the dimensions of the factor matrices).

![Error versus Compression](graphics/error-vs-compression.png)

Next, we compare the compressed representations to the original by visualizing sets of slices.
The compressed representations are fairly accurate up to 155X compression (tolerance of 1e-2). 
Note that we have used the same colormaps for each feature across all images. 

Below are the same set of xy slices as above.

![Comparison of xy slices](graphics/compare-xy-slices.png)

Below are the same set of xz slices as above.

![Comparison of xz slices](graphics/compare-xz-slices.png)

## Questions for Consideration/Numerical Evaluation

* Would shifting/rescaling the data before compression be useful?
* Can we visualize the regions with the highest errors?
* What if we specify the ranks rather than the errors?

## Source & Preprocessing

Due to its size, we do not include the raw original data here but only the extracted core density MAT file.
In the `original_data` directory, we include scripts for downloading the original data and extracting the subtensor.
The bash script `generate_miranda.sh` uses the Matlab script `extract_miranda_subtensor.m` for part of the process of reproducing the `density.mat` file.
We provide here some information about the original dataset. 

* We use the *Miranda* dataset from [SDRBench](https://sdrbench.github.io/).
* The original data (48 GB) was downloaded from [this URL](https://g-8d6b0.fd635.8443.data.globus.org/ds131.2/Data-Reduction-Repo/raw-data/Miranda/SDRBENCH-Miranda-3072x3072x3072.tar.gz). 
* [Metadata provided by SDRBench](original_data/dataset_info.txt)
* Example publications using this dataset:
  - P. Lindstrom, Fixed-Rate Compressed Floating-Point Arrays, IEEE Trans. Visualization and Computer Graphics, Vol. 20, No. 12, pp. 2674-2683, 2014, https://doi.org/10.1109/TVCG.2014.2346458
  - P. Lindstrom, and M. Isenburg, Fast and Efficient Compression of Floating-Point Data, IEEE Trans. Visualization and Computer Graphics, Vol. 12, No. 5, pp. 1245-1250, 2006, https://doi.org/10.1109/TVCG.2006.143

