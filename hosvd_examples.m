%% load data and convert to tensor
load density
X = tensor(density);
normX = norm(X);

%% compute Tucker decompositions for various error thresholds
tols = [1e-1,1e-2,1e-3,1e-4];
ntols = length(tols);
T = cell(ntols,1);
coresizes = zeros(ntols,ndims(X));
compratios = zeros(ntols,1);
comppercs = zeros(ntols,1);
relerrs = zeros(ntols,1);
for i = 1:ntols
    S = hosvd(X,tols(i));
    coresizes(i,:) = size(S.core);
    compratios(i) = whos('X').bytes / whos('S').bytes;
    comppercs(i) = 100 / compratios(i);
    relerrs(i) = sqrt(normX^2-norm(S.core)^2)/normX;
    T{i} = S;
end

%% plot errors vs compression ratios
figure, loglog(relerrs,compratios,'-*')
xlabel('Relative Error')
ylabel('Compression Ratio')
axis([8e-5 1.25e-1 1 3e4])

% label points
for i = 1:ntols
    t = text(relerrs(i),compratios(i),sprintf('(%0.0e,%0.0f)',relerrs(i),compratios(i)));
    set(t,'VerticalAlignment','top')
    if i == 1
        set(t,'HorizontalAlignment','right')
        set(t,'VerticalAlignment','bottom')
    end
end

% export as png
width = 3;     % Width in inches
height = 3;    % Height in inches
pos = get(gcf, 'Position');
delta_y = max(0, height*100 - pos(4));
set(gcf, 'Position', [pos(1) (pos(2)-delta_y) width*100, height*100]);
exportgraphics(gcf,'graphics/error-vs-compression.png')

%% plot xy slices for visual comparison
zs = 64:384:2048;
limits = [min(density,[],'all') max(density,[],'all')];
figure, tiledlayout(1+ntols,length(zs),'TileSpacing','Compact');
for i = 1:ntols+1
    for z = zs
        if i == 1
            nexttile, imagesc(squeeze(density(z,:,:)));
            title('Original: z = ' + string(z))
        else
            nexttile, imagesc(double(squeeze(reconstruct(T{6-i},1,z)))); % reconstruct
            title('Tol ' + compose("%.e",tols(6-i)) + ': z = ' + string(z))
        end
        axis square
        clim(limits)
        
    end
end
height = 200*(1+ntols); width = 200*length(zs); 
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100], 'colormap', jet);
exportgraphics(gcf,'graphics/compare-xy-slices.png');

%% plot xz slices for visual comparison
ys = 16:48:256;
limits = [min(density,[],'all') max(density,[],'all')];
figure, tiledlayout(1+ntols,length(ys),'TileSpacing','Compact');
for i = 1:ntols+1
    for y = ys
        if i == 1
            nexttile, imagesc(squeeze(density(:,y,:)));
            title('Original: y = ' + string(y))
        else
            nexttile, imagesc(squeeze(double(reconstruct(T{6-i},2,y)))); % reconstruct
            title('Tol ' + compose("%.e",tols(6-i)) + ': y = ' + string(y))
        end
        clim(limits)
        
    end
end
height = 16*(1+ntols); width = 2*length(zs); 
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100], 'colormap', jet);
exportgraphics(gcf,'graphics/compare-xz-slices.png');