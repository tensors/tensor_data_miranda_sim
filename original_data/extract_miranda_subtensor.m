n = 3072; % original xy dim
xy = 256; % desired xy dim
z = 2048; % z dim
fid = fopen('density.f32');
X = fread(fid,'*single'); % read in single precision
X = reshape(X,[n,n,z]); % reshape to 3D array
l = floor((n-xy)/2);
density = double(X(l:l+xy-1,l:l+xy-1,:)); % extract and convert to double precision
density = permute(density,[3 2 1]); % reorient tensor from (x,y,z) to (i,j,k)
density = flip(density,3);
density = flip(density,1);
save('density','density'); % save as mat file

