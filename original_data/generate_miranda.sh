#!/bin/bash

## This script downloads the original Miranda dataset (the big version) 
#  and extracts a subtensor from the middle of the domain.
#  It requires wget, gunzip, tar, and MATLAB.

# download original dataset (48 GB), can take several minutes
echo "Downloading original dataset (48 GB), can take several minutes"
wget https://g-8d6b0.fd635.8443.data.globus.org/ds131.2/Data-Reduction-Repo/raw-data/Miranda/SDRBENCH-Miranda-3072x3072x3072.tar.gz

# unzip and extract (108 GB), can take several minutes
echo "Unzipping and extracting data files, can take several minutes"
tar -xvf SDRBENCH-Miranda-3072x3072x3072.tar.gz

# concatenate files (72 GB), ignoring top and bottom slabs, can take several minutes
echo "Concatenating files, can take several minutes"
touch density.f32
for z in {16..79}
do
   echo "Concatenating file #$z"
   cat SDRBENCH-Miranda-3072x3072x3072/density-$z.f32 >> density.f32
done


# run matlab script to extract core and convert to double precision (1 GB)
echo "Using MATLAB to extract core subtensor of size 1 GB"
matlab -batch "extract_miranda_subtensor"
mv density.mat ../.

# clean up raw data
echo "Deleting raw data"
rm SDRBENCH-Miranda-3072x3072x3072.tar.gz
rm -rf SDRBENCH-Miranda-3072x3072x3072
rm density.f32

echo "Success!  Extracted subtensor of available as density.mat"

